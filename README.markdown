The build.elastic.co API
========================

{TODO DESCRIPTION}

## Running the application

Install the Rubygems:

    bundle install

Run the application in development:

    bundle exec rake server

Run the test suite:

    bundle exec rake test

-----

Copyright (c) 2016 Elasticsearch <http://elastic.co>
