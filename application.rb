$LOAD_PATH.unshift File.expand_path('../lib', __FILE__)

require 'bundler'
Bundler.require(:default, ENV['RACK_ENV'])

require 'models/project'
require 'models/build'
require 'models/timeline'

module Elastic
  module CI
    module API
      class Application < Sinatra::Base
        enable :logging

        configure :development do
          enable  :dump_errors
          enable  :show_exceptions

          require  'sinatra/reloader'
          register Sinatra::Reloader
        end

        helpers do
          def json(value)
            Oj.dump value, mode: :compat, indent: 2
          end

          def to_array(value)
            value.to_s.strip.to_s.split(/,\s*/)
          end
        end

        before do
          content_type :json
        end

        options '/*' do
          response["Access-Control-Allow-Methods"] = "OPTIONS, HEAD, GET, POST, PUT, DELETE"
          response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type, Content-Length"
          response["Access-Control-Allow-Origin"]  = "*"
        end

        get '/' do
          json('message' => 'Welcome to the build.elastic.co API!')
        end

        # Get a list of projects
        #
        #     /projects
        #
        get '/projects' do
          json Project.all.as_json
        end

        # Get a single project
        #
        #     /project/{id}{?include}
        #
        # + Parameters
        #
        #    + id:      The ID of the project
        #
        #    + include: Nested resources to include with the basic info.
        #               Possible values: `builds`
        #
        get '/project/:id' do |id|
          json Project.find(id, include: to_array(params[:include])).as_json
        end

        # Get a list of builds
        #
        # By default, returns all builds within the last 30 days (up to 10,000).
        # Can be filtered for project, status or start and stop time (ISO8601 formatted).
        #
        #     /builds/{?start,stop,project_id,status}
        #
        # + Parameters
        #
        #    + start:      Start time to limit the period of builds to return
        #    + stop:       Stop time to limit the period of builds to return
        #    + project_id: The project ID to filter the builds
        #    + status:     The build status to filter the builds
        #
        get '/builds' do
          valid_params = params.select { |k,v| ['project_id', 'status'].include?(k) }
          json Build.find(valid_params).as_json
        end

        # Get a single build
        #
        #     /build/{id}
        #
        # + Parameters
        #
        #    + id: The ID of the build
        #
        get '/build/:id' do |id|
          json Build.find(id).as_json
        end

        # Get the aggregated statistics for a time period
        #
        #     /timeline{?start,stop,granularity,project_ids,properties,include}
        #
        # + Parameters
        #
        #    + start:      Start time to limit the period of statistics to return
        #    + stop:       Stop time to limit the period of statistics to return
        #    + granularity: The granularity of the period (day, week, month)
        #    + project_ids: Comma delimited list of project IDs to filter the statistics
        #    + properties:  Additional metrics to return.
        #                   Possible values: `sys.os`
        #
        get '/timeline' do
          valid_params = params.select { |k,v|
            %w[ start stop granularity project_ids include properties ].include?(k)
          }
          valid_params['project_ids'] = to_array(params['project_ids']) unless params['project_ids'].nil?

          json Timeline.new(valid_params).as_json
        end
      end
    end
  end
end
