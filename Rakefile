#!/usr/bin/env rake

require 'elasticsearch/extensions/test/cluster'

task :default do
  exec "rake --tasks"
end

require 'rake/testtask'

Rake::TestTask.new('test:unit') do |test|
  test.libs << 'test'
  test.test_files = FileList['test/unit/**/*_test.rb']
  test.verbose = true
  # test.warning = true
end

Rake::TestTask.new('test:integration') do |test|
  test.libs << 'test'
  test.test_files = FileList['test/integration/**/*_test.rb']
  test.verbose = true
  # test.warning = true
end

Rake::TestTask.new('test:all') do |test|
  test.libs << 'test'
  test.test_files = FileList['test/**/*_test.rb']
  test.verbose = true
  # test.warning = true
end

namespace :application do
  desc "Start server for development"
  task :start do
    exec "thin -R config.ru start"
  end
end

namespace :elasticsearch do
  desc "Start Elasticsearch node for tests"
  task :start do
    require 'elasticsearch/extensions/test/cluster'
    Elasticsearch::Extensions::Test::Cluster.start(port: ENV.fetch('TEST_CLUSTER_PORT', 9250), nodes: 1, path_logs: '/tmp')
  end

  desc "Stop Elasticsearch node for tests"
  task :stop do
    require 'elasticsearch/extensions/test/cluster'
    Elasticsearch::Extensions::Test::Cluster.stop(port: ENV.fetch('TEST_CLUSTER_PORT', 9250), nodes: 1)
  end

  task :status do
    require 'elasticsearch/extensions/test/cluster'
    begin
      Elasticsearch::Extensions::Test::Cluster.__print_cluster_info(ENV.fetch('TEST_CLUSTER_PORT', 9250))
    rescue Errno::ECONNREFUSED
      puts "\e[31m[!] Test cluster not running\e[0m"; exit(1)
    end
  end
end
