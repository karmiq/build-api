#\ --port 3000 --server thin

require './application'

map '/' do
  run Elastic::CI::API::Application
end
