require 'models/base'

module Elastic
  module CI
    module API

      class Build
        include Base

        def self.find(criteria={})
          default_start = (Time.now.utc -  24*60*60*7).iso8601 # 7 days
          default_stop  = Time.now.utc.iso8601

          case criteria
            when :all
              start = default_start
              stop  = default_stop
            when Hash
              start = criteria[:start] || criteria['start'] || default_start
              stop  = criteria[:stop]  || criteria['stop']  || default_stop
              project_id = criteria[:project_id] || criteria['project_id']
              status     = criteria[:status] || criteria['status']
            when String
              build_id = criteria
            else
              raise ArgumentError, "Expected one of: :all, Hash, String, got #{criteria.class}"
          end

          response = client.search(
            index: 'build-*',
            type: '',
            body: Elasticsearch::DSL::Search::Search.new do
              query do
                filtered do
                  filter do
                    bool do
                      if build_id.nil?
                        must { term 'build.project' => project_id } unless project_id.nil?
                        must { term 'process.status' => status }    unless status.nil?

                        must { range 'process.time-start' => { gte: start, lte: stop } }
                      else
                        must { term 'id' => build_id }
                      end
                    end
                  end
                end
              end

              sort 'process.time-start' => 'desc'
              size  10_000
            end
          )

          if build_id.nil?
            Collection.new(response['hits']['hits'].map do |hit|
              Build.new(__extract_attributes(hit))
            end)
          else
            if hit = response['hits']['hits'][0]
              Build.new(hit['_source'].merge __extract_attributes(hit))
            else
              nil
            end
          end
        end

        def self.__extract_attributes(hit)
          { id: hit['_id'],
            time_start: hit['_source']['process']['time-start'],
            time_end: hit['_source']['process']['time-end'],
            status: hit['_source']['process']['status'],
            html_url: hit['_source']['build']['url'] }
        end
      end

    end
  end
end
