require 'models/base'

module Elastic
  module CI
    module API

      class Timeline
        include Base

        attr_reader :project_ids, :start, :stop, :granularity, :properties

        def initialize(options={})
          default_start = (Time.now.utc -  24*60*60*30).iso8601 # 30 days
          default_stop  = Time.now.utc.iso8601
          default_granularity = 'day'

          @project_ids = Array(options[:project_ids] || options['project_ids'])
          @start = options[:start] || options['start'] || default_start
          @stop  = options[:stop]  || options['stop']  || default_stop
          @granularity = options[:granularity] || options['granularity'] || default_granularity
          @properties = Array(options[:properties] || options['properties'])
        end

        def __load_data
          # KLUDGE: Figure out how to pass variables to the DSL block
          project_ids = @project_ids
          start       = @start
          stop        = @stop
          granularity = @granularity
          properties  = @properties

          search_definition = Elasticsearch::DSL::Search::Search.new do
            size 0
            query do
              filtered do
                filter do
                  bool do
                    must do
                      terms 'build.project' => project_ids
                    end unless project_ids.empty?

                    must do
                      range 'process.time-start' do
                        gte "#{start}||/d"
                        lte "#{stop}||/d"
                      end
                    end
                  end
                end

                query { match_all }
              end
            end

            aggregation :projects do
              # KLUDGE: Repeated definition of aggregations
              if project_ids.empty?
                filter do
                  aggregation :_all do
                    filter do
                      aggregation :builds_per_granularity do
                        date_histogram do
                          field 'process.time-start'
                          interval granularity
                          format 'yyyy-MM-dd'

                          aggregation :success_failure do
                            terms do
                              field 'process.status'
                              order _term: 'desc'
                            end
                          end

                          aggregation :avg_duration do
                            avg field: 'process.took'
                          end

                          aggregation 'sys.os' do
                            terms do
                              field 'sys.os'
                            end
                          end if properties.include?('sys.os')
                        end
                      end
                    end
                  end
                end
              else
                terms do
                  field 'build.project'
                  size 1000
                  order _term: 'asc'

                  aggregation :builds_per_granularity do
                    date_histogram do
                      field 'process.time-start'
                      interval granularity
                      format 'yyyy-MM-dd'

                      aggregation :success_failure do
                        terms do
                          field 'process.status'
                          order _term: 'desc'
                        end
                      end

                      aggregation :avg_duration do
                        avg field: 'process.took'
                      end

                      aggregation 'sys.os' do
                        terms do
                          field 'sys.os'
                        end
                      end if properties.include?('sys.os')
                    end
                  end
                end
              end
            end
          end

          response = client.search(
            index: 'build-*',
            type: '',
            search_type: 'count',
            body: search_definition
          )

          transform = lambda do |bucket|
            date = bucket['key_as_string']
            builds = bucket['doc_count']

            success = -> { s = bucket['success_failure']['buckets'].select { |d| d['key'] == 'SUCCESS' }.first; s ? s['doc_count'] : 0 }.call
            failure = -> { s = bucket['success_failure']['buckets'].select { |d| d['key'] == 'FAILURE' }.first; s ? s['doc_count'] : 0 }.call
            avg_duration = bucket['avg_duration']['value'].to_i

            r = { date: date, builds: builds, success: success, failure: failure, avg_duration: avg_duration }
            r['sys.os'] = bucket['sys.os']['buckets'].map { |b| { name: b['key'], count: b['doc_count'] } } if properties.include?('sys.os')
            r
          end

          result = if response['aggregations']['projects']['_all']
            [{ project_id: '_all', builds: response['aggregations']['projects']['_all']['builds_per_granularity']['buckets'].map(&transform) }]
          else
            response['aggregations']['projects']['buckets'].map do |project_bucket|
              { project_id: project_bucket['key'], builds: project_bucket['builds_per_granularity']['buckets'].map(&transform) }
            end
          end

          result
        end

        def to_hash
          __load_data
        end; alias_method :as_json, :to_hash
      end

    end
  end
end
