require 'logger'
require 'hashie/mash'
require 'elasticsearch'
require 'elasticsearch/dsl'

module Elastic
  module CI
    module API

      module Base
        def self.included(base)
          base.__send__ :extend, Elasticsearch::DSL::Search

          base.__send__ :include, InstanceMethods
          base.__send__ :extend, ClassMethods

          base.class_eval do
            attr_reader :attributes

            def initialize(attributes={})
              @attributes = Hashie::Mash.new(attributes)
            end
          end
        end

        class Collection < Array
          def as_json(options={})
            self.map { |d| d.as_json }
          end
        end

        module InstanceMethods
          def client
            self.class.client
          end

          def to_hash
            @attributes.to_hash
          end

          alias_method :as_json, :to_hash
        end

        module ClassMethods
          def client
            @client ||= begin
              client = ::Elasticsearch::Client.new
              # client.transport.logger = Logger.new(STDERR)
              client
            end
          end
        end
      end

    end
  end
end
