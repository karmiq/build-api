require 'models/base'
require 'models/build'

module Elastic
  module CI
    module API

      class Project
        include Base

        def self.all(options={})
          response = client.search(
            index: 'build-*',
            type: '',
            search_type: 'count',
            body: Elasticsearch::DSL::Search::Search.new do
              query { match_all }
              size 0

              aggregation :projects do
                terms do
                  field 'build.project'
                  size 1000
                  order _term: 'asc'

                  aggregation :github_urls do
                    terms do
                      field 'vcs.project-url'
                    end
                  end
                end
              end
            end
          )

          if response['aggregations'] && response['aggregations']['projects'] && response['aggregations']['projects']['buckets']
            Collection.new(response['aggregations']['projects']['buckets'].map do |bucket|
              Project.new id: bucket['key'], github_url: bucket['github_urls']['buckets'][0]['key']
            end)
          else
            Collection.new([])
          end
        end

        def self.find(id, options={})
          result = all.select { |d| d.attributes.id == id }.first
          result.instance_variable_get(:@attributes).merge!(builds: result.builds) if options[:include] && options[:include].include?('builds')
          result
        end

        def builds(options={})
          Build.find(options.merge(project_id: self.attributes.id))
        end
      end

    end
  end
end
