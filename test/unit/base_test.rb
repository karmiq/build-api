require '_helper'

require 'models/build'

module Elastic
  module CI
    module API
      class BaseModuleTest < Minitest::Test
        class DummyModel
          include Base
        end

        context "The base module for models" do
          should "be initialized with attributes" do
            subject = DummyModel.new(foo: 'bar')

            assert_equal 'bar', subject.attributes[:foo]
            assert_equal 'bar', subject.attributes.foo
          end

          should "have a client" do
            refute_nil DummyModel.client
            refute_nil DummyModel.new.client

            assert_respond_to DummyModel.client, :info
          end

          should "include the Elasticsearch Ruby DSL module" do
            assert_respond_to DummyModel, :search
          end
        end
      end
    end
  end
end
