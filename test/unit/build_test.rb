require '_helper'

require 'models/build'

module Elastic
  module CI
    module API
      class BuildTest < Minitest::Test
        DEFAULT_RESPONSE = {
           "took"=>123,
           "timed_out"=>false,
           "_shards"=>{"total"=>8, "successful"=>8, "failed"=>0},
           "hits"=>
            {"total"=>42,
             "max_score"=>nil,
             "hits"=>
              [
                {"_index"=>"build-1453259317148",
                "_type"=>"t",
                "_id"=>"20160323131706-4441A077",
                "_score"=>nil,
                "_source"=>
                 {"id"=>"20160323131706-4441A077",
                  "version"=>{"string"=>"0.99.35", "hash"=>"ef9f476d5f18fa23ff0d3503394b6785c65b2795"},
                  "java"=>
                   {"class"=>{"path"=>"/usr/local/bin/runbld"},
                    "runtime"=>{"name"=>"OpenJDK Runtime Environment", "version"=>"1.8.0_72-internal-b05"},
                    "vendor"=>"Oracle Corporation",
                    "version"=>"1.8.0_72-internal",
                    "vm"=>{"info"=>"mixed mode", "name"=>"OpenJDK 64-Bit Server VM", "vendor"=>"Oracle Corporation", "version"=>"25.72-b05"},
                    "home"=>"/usr/lib/jvm/java-8-openjdk-amd64"},
                  "vcs"=>
                   {"commit-time"=>"2016-03-23T12:31:50.000Z",
                    "author-name"=>"Simon Willnauer",
                    "author-email"=>"simonw@apache.org",
                    "commit-name"=>"Simon Willnauer",
                    "commit-url"=>"https://github.com/elastic/elasticsearch/commit/4826782b377475171441bd8bfde24965d1a6f1db",
                    "log-pretty"=>
                     "commit 4826782b377475171441bd8bfde24965d1a6f1db\nAuthor: Simon Willnauer <simonw@apache.org>\nDate:   2016-03-23T12:31:50.000Z\n\nwrap line at 140chars",
                    "commit-id"=>"4826782b377475171441bd8bfde24965d1a6f1db",
                    "message-full"=>"wrap line at 140chars\n",
                    "commit-email"=>"simonw@apache.org",
                    "branch-url"=>"https://github.com/elastic/elasticsearch/tree/master",
                    "project-url"=>"https://github.com/elastic/elasticsearch",
                    "provider"=>"git",
                    "author-time"=>"2016-03-23T12:31:50.000Z",
                    "commit-short"=>"4826782",
                    "message"=>"wrap line at 140chars"},
                  "sys"=>
                   {"kernel-name"=>"Linux",
                    "ram-gb"=>15.67,
                    "cpus-physical"=>1,
                    "timezone"=>"UTC",
                    "fs-percent-free"=>70.413,
                    "fs-bytes-total"=>211233755136,
                    "uptime-secs"=>7236589,
                    "uptime"=>"83 days",
                    "instance-type"=>"m4.xlarge",
                    "arch"=>"amd64",
                    "image-id"=>"ami-c03029a1",
                    "hostname"=>"slave-314de7eb",
                    "facter-version"=>"3.1.3",
                    "cpu-type"=>"Intel(R) Xeon(R) CPU E5-2676 v3 @ 2.40GHz",
                    "instance-id"=>"i-314de7eb",
                    "region"=>"us-west-2",
                    "fs-bytes-used"=>62498770944,
                    "os-version"=>"14.04",
                    "fs-mountpoint"=>"rootfs",
                    "ip4"=>"10.34.171.216",
                    "fs-percent-used"=>29.587,
                    "fs-type"=>"rootfs",
                    "ip6"=>"fe80::842:40ff:fe39:d47f",
                    "kernel-version"=>"3.13.0",
                    "ram-mb"=>16047.98,
                    "virtual"=>true,
                    "uptime-days"=>83,
                    "os"=>"Ubuntu",
                    "fs-bytes-free"=>148734984192,
                    "provider"=>"aws-ec2",
                    "datacenter"=>"us-west-2c",
                    "kernel-release"=>"3.13.0-74-generic",
                    "facter-provider"=>"facter",
                    "model"=>"x86_64",
                    "cpus"=>4},
                  "build"=>
                   {"console-url"=>
                     "https://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+master+multijob-os-compatibility/os=ubuntu/109/console",
                    "scheduler"=>"jenkins",
                    "tags"=>["linux", "slave-314de7eb", "swarm", "ubuntu", "ubuntu-14.04"],
                    "org-project-branch"=>"elastic/elasticsearch#master",
                    "job-name"=>"elastic+elasticsearch+master+multijob-os-compatibility/os=ubuntu",
                    "number"=>"109",
                    "node"=>"slave-314de7eb",
                    "org"=>"elastic",
                    "project"=>"elasticsearch",
                    "url"=>"https://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+master+multijob-os-compatibility/os=ubuntu/109/",
                    "executor"=>"0",
                    "job-name-extra"=>"multijob-os-compatibility/os=ubuntu",
                    "branch"=>"master"},
                  "process"=>
                   {"took"=>360815,
                    "exit-code"=>1,
                    "out-bytes"=>353356,
                    "total-bytes"=>360078,
                    "status"=>"FAILURE",
                    "cmd-source"=>"#!/usr/local/bin/runbld\ngradle --stacktrace clean\ngradle --info build\n",
                    "time-end"=>"2016-03-23T13:23:07.928Z",
                    "millis-start"=>1458739027113,
                    "cmd"=>["bash", "-x", "/tmp/hudson2936435333541338218.sh"],
                    "err-bytes"=>6722,
                    "millis-end"=>1458739387928,
                    "time-start"=>"2016-03-23T13:17:07.113Z"},
                  "test"=>
                   {"errors"=>0,
                    "failures"=>1,
                    "tests"=>4389,
                    "skipped"=>17,
                    "failed-testcases"=>
                     [{"error-type"=>"failure",
                       "class"=>"org.elasticsearch.index.mapper.core.StringMappingUpgradeTests",
                       "test"=>"testUpgradeFielddataSettings",
                       "type"=>"java.lang.AssertionError",
                       "message"=>"expected:<false> but was:<true>",
                       "summary"=>"FAILURE StringMappingUpgradeTests testUpgradeFielddataSettings"}]}},
                "sort"=>[1458739027113]}
              ]
            }
          }


        context "Build" do
          should "return all builds" do
            Build.client
              .expects(:search)
              .returns(DEFAULT_RESPONSE)

            result = Build.find(:all)

            assert_instance_of Base::Collection, result
            assert_equal 1, result.size
            assert_equal 'FAILURE', result.first.attributes.status
          end

          should "return builds for a project" do
            Build.client
              .expects(:search)
              .returns(DEFAULT_RESPONSE)

            result = Build.find(project_id: 'foo')

            assert_instance_of Base::Collection, result
            assert_equal 1, result.size
            assert_equal 'FAILURE', result.first.attributes.status
            assert_nil result.first.attributes.vcs
          end

          should "return a single build" do
            Build.client
              .expects(:search)
              .returns(DEFAULT_RESPONSE)

            result = Build.find('foo')

            assert_instance_of Build, result
            assert_equal 'FAILURE', result.attributes.status
            assert_equal 'FAILURE', result.attributes.process.status
            assert_equal 'Simon Willnauer', result.attributes.vcs['author-name']
          end

          should "raise exception for incompatible argument" do
            assert_raises ArgumentError do
              Build.find(true)
            end
          end
        end
      end
    end
  end
end
