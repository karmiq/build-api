require '_helper'

require 'models/project'

module Elastic
  module CI
    module API
      class ProjectTest < Minitest::Test
        DEFAULT_RESPONSE = {
          'aggregations' => {
            'projects' => {
              'buckets' => [
                { 'key' => 'foo', 'github_urls' => { 'buckets' => [ {'key' => 'http://foo'} ] } },
                { 'key' => 'bar', 'github_urls' => { 'buckets' => [ {'key' => 'http://bar'} ] } },
              ]
            }
          }
        }

        context "Project" do
          should "return all projects" do
            Project.client
              .expects(:search)
              .returns(DEFAULT_RESPONSE)

            assert_equal 2, Project.all.size
          end

          should "return a single project" do
            Project.client
              .expects(:search)
              .returns(DEFAULT_RESPONSE)

            assert_instance_of Project, Project.find('foo')
          end

          should "return a single project with builds" do
            Project.client
              .expects(:search)
              .returns(DEFAULT_RESPONSE)

            Build.expects(:find)
              .with do |options|
                assert_equal 'foo', options[:project_id]
              end
              .returns(Build::Collection.new([]))

            refute_nil Project.find('foo', include: 'builds').attributes[:builds]
          end
        end

      end
    end
  end
end
