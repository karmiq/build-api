require '_helper'

require 'rack/test'

require_relative '../../application'

module Elastic
  module CI
    module API
      class ApplicationTest < Minitest::Test

        include Rack::Test::Methods
        alias :response :last_response

        def app
          Elastic::CI::API::Application.new
        end

        def json(value)
          Oj.dump value, indent: 2
        end

        context "Application" do
          context "helpers" do
            should "encode to JSON" do
              assert_respond_to app.helpers, :json
            end

            should "convert comma-delimited strings to arrays" do
              assert_equal [], app.helpers.to_array(nil)
              assert_equal [], app.helpers.to_array('')
              assert_equal [], app.helpers.to_array(' ')
              assert_equal [], app.helpers.to_array(',')
              assert_equal [], app.helpers.to_array(' , ')
              assert_equal ['foo'], app.helpers.to_array('foo')
              assert_equal ['foo'], app.helpers.to_array('foo,')
              assert_equal ['foo', 'bar'], app.helpers.to_array('foo,bar')
              assert_equal ['foo', 'bar'], app.helpers.to_array('foo, bar')
              assert_equal ['foo', 'bar'], app.helpers.to_array(' foo, bar')
              assert_equal ['foo', 'bar'], app.helpers.to_array('    foo,  bar  ')
              assert_equal ['foo', 'bar'], app.helpers.to_array('foo,bar,')
            end
          end

          should "return JSON content type by default" do
            get '/'
            assert_equal 'application/json', response.content_type
          end

          should "respond to CORS preflight requests" do
            options '/'
            assert_match /X-Requested-With/, response.headers['Access-Control-Allow-Headers']
          end

          should "display welcome message on root" do
            get '/'
            assert response.ok?, response.status.to_s
            assert_match /"message"\:/, response.body
          end

          should "display a list of projects" do
            Project.expects(:all).returns(Project::Collection.new([]))
            get '/projects'
            assert response.ok?, response.status.to_s
          end

          should "display single project" do
            Project.expects(:find)
              .with do |id, options|
                assert_equal 'foo', id
              end
              .returns(Project.new({id: 'foo'}))

            get '/project/foo'
            assert response.ok?, response.status.to_s
          end

          should "include builds in the project" do
            Project.expects(:find)
              .with do |id, options|
                  assert_equal 'foo', id
                  assert_equal ['builds'], options[:include]
                end
              .returns(Project.new({id: 'foo', builds: Build::Collection.new([])}))

            get '/project/foo?include=builds'
            assert response.ok?, response.status.to_s
          end

          should "display builds" do
            Build.expects(:find)
              .with({})
              .returns(Build::Collection.new([]))

            get '/builds?foo=bar'
            assert response.ok?, response.status.to_s
          end

          should "display builds filtered with criteria" do
            Build.expects(:find)
              .with({'status' => 'SUCCESS'})
              .returns(Build::Collection.new([]))

            get '/builds?status=SUCCESS&bogus=hack'
            assert response.ok?, response.status.to_s
          end

          should "display single build" do
            Build.expects(:find).with('foobar').returns(Build.new)

            get '/build/foobar'
            assert response.ok?, response.status.to_s
          end

          should "display timeline statistics for all projects" do
            Timeline.expects(:new).with({}).returns(mock(as_json: {}))
            get '/timeline'
            assert response.ok?, response.status.to_s
          end

          should "display timeline statistics for specific projects" do
            Timeline.expects(:new).with({'project_ids' => ['foo', 'bar']}).returns(mock(as_json: {}))
            get '/timeline?project_ids=foo,bar'
            assert response.ok?, response.status.to_s
          end

          should "allow to customize the granularity of the timeline statistics" do
            Timeline.expects(:new).with({'granularity' => 'month'}).returns(mock(as_json: {}))
            get '/timeline?granularity=month'
            assert response.ok?, response.status.to_s
          end
        end
      end
    end
  end
end
