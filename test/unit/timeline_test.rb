require '_helper'

require 'models/timeline'

module Elastic
  module CI
    module API
      class TimelineTest < Minitest::Test
        RESPONSE_COMBINED = {"aggregations"=>
          {"projects"=>
            {"doc_count"=>6566,
             "_all"=>
              {"doc_count"=>6566,
               "builds_per_granularity"=>
                {"buckets"=>
                  [{"key_as_string"=>"2016-02-24",
                    "key"=>1456272000000,
                    "doc_count"=>90,
                    "success_failure"=>
                     {"doc_count_error_upper_bound"=>0,
                      "sum_other_doc_count"=>0,
                      "buckets"=>[{"key"=>"SUCCESS", "doc_count"=>88}, {"key"=>"FAILURE", "doc_count"=>2}]},
                    "avg_duration"=>{"value"=>100000.4}},

                   {"key_as_string"=>"2016-02-25",
                    "key"=>1456358400000,
                    "doc_count"=>126,
                    "success_failure"=>
                     {"doc_count_error_upper_bound"=>0,
                      "sum_other_doc_count"=>0,
                      "buckets"=>[{"key"=>"SUCCESS", "doc_count"=>118}, {"key"=>"FAILURE", "doc_count"=>8}]},
                    "avg_duration"=>{"value"=>109000.8}},

                   {"key_as_string"=>"2016-02-26",
                    "key"=>1456444800000,
                    "doc_count"=>191,
                    "success_failure"=>
                     {"doc_count_error_upper_bound"=>0,
                      "sum_other_doc_count"=>0,
                      "buckets"=>[{"key"=>"SUCCESS", "doc_count"=>186}, {"key"=>"FAILURE", "doc_count"=>5}]},
                    "avg_duration"=>{"value"=>110000.0}}
                   ]
                }
              }
            }
          }
        }

        RESPONSE_PROJECTS = {"aggregations"=>
          {"projects"=>
            {"buckets"=>
              [{"key"=>"foo",
                "doc_count"=>4398,
                "builds_per_granularity"=>
                 { "buckets"=>
                   [{"key_as_string"=>"2016-02-23",
                     "key"=>1456185600000,
                     "doc_count"=>85,
                     "success_failure"=>
                      {"buckets"=>[{"key"=>"SUCCESS", "doc_count"=>84}, {"key"=>"FAILURE", "doc_count"=>1}]},
                     "avg_duration"=>{"value"=>100000.4}}]
                  }},

               {"key"=>"bar",
                "doc_count"=>1492,
                "builds_per_granularity"=>
                 {"buckets"=>
                   [{"key_as_string"=>"2016-02-23",
                     "key"=>1456185600000,
                     "doc_count"=>48,
                     "success_failure"=>
                      {"buckets"=>[{"key"=>"SUCCESS", "doc_count"=>48}]},
                     "avg_duration"=>{"value"=>100000.4}}]
                  }}
              ]
            }
          }
        }

        context "Timeline statistics" do
          should "allow to be initialized without options" do
            subject = Timeline.new

            assert_equal [], subject.project_ids
            assert_match(Regexp.new(Time.now.utc.strftime('%Y-%m-%d')), subject.stop)
          end

          should "be initialized with options" do
            subject = Timeline.new(project_ids: ['foo', 'bar'], start: '2015-01-01', stop: '2015-01-30')

            assert_equal ['foo', 'bar'], subject.project_ids
            assert_equal '2015-01-30', subject.stop
          end

          should "return the combined timeline" do
            Timeline.client
              .expects(:search)
              .returns(RESPONSE_COMBINED)

            result = Timeline.new.as_json

            assert_equal ['_all'], result.map { |r| r[:project_id] }

            assert_equal 3, result[0][:builds].size
            assert_equal 2, result[0][:builds][0][:failure]
          end

          should "return timeline for specific projects" do
            Timeline.client
              .expects(:search)
              .returns(RESPONSE_PROJECTS)

            result = Timeline.new(project_ids: ['foo', 'bar']).as_json

            assert_equal ['foo', 'bar'], result.map { |r| r[:project_id] }

            assert_equal 1, result[0][:builds].size
            assert_equal 1, result[0][:builds][0][:failure]
            assert_equal 1, result[1][:builds].size
            assert_equal 0, result[1][:builds][0][:failure]
          end
        end
      end
    end
  end
end
