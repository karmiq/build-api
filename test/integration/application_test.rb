at_exit { Elastic::CI::API::ApplicationIntegrationTest.__run_at_exit_hooks }

require 'logger'
require '_helper'

require 'rack/test'

require_relative '../../application'

module Elastic
  module CI
    module API
      class ApplicationIntegrationTest < Minitest::Test
        extend Elasticsearch::Extensions::Test::StartupShutdown

        include Rack::Test::Methods
        alias :response :last_response

        def app
          Elastic::CI::API::Application.new
        end

        startup do
          Elasticsearch::Extensions::Test::Cluster.start(port: ENV.fetch('TEST_CLUSTER_PORT', 9250), nodes: 1, path_logs: '/tmp') if ENV['SERVER']
        end

        shutdown do
          Elasticsearch::Extensions::Test::Cluster.stop(port: ENV.fetch('TEST_CLUSTER_PORT', 9250)) if ENV['SERVER']
        end

        context "Application" do
          setup do
            Time.stubs(:now).returns(Time.parse("2016-03-25T22:00:00+01:00"))

            @port  = ENV.fetch('TEST_CLUSTER_PORT', 9250).to_i
            @client = Elasticsearch::Client.new url: "http://127.0.0.1:#{@port}"

            @logger =  Logger.new(STDERR)
            @logger.formatter = proc do |severity, datetime, progname, msg|
              color = case severity
                when /INFO/ then :green
                when /ERROR|WARN|FATAL/ then :red
                when /DEBUG/ then :cyan
                else :white
              end
              ANSI.ansi(severity[0] + ' ', color, :faint) + ANSI.ansi(msg, :white, :faint) + "\n"
            end

            builds = MultiJson.load(File.open(FIXTURES.join('builds_elasticsearch.json'))) +
                     MultiJson.load(File.open(FIXTURES.join('builds_lucene.json')))

            @client.indices.delete index: 'build-*', ignore: [404]
            @client.indices.create index: 'build-test', body: {
              mappings: {
                builds.first['_type'].to_sym => {
                  properties: MultiJson.load(File.open(FIXTURES.join('build-mappings.json')))
                }
              }
            }

            builds.each do |d|
              @client.index index: 'build-test', type: d['_type'], id: d['_d'], body: d['_source']
            end

            @client.indices.refresh index: 'build-test'

            [Elastic::CI::API::Project, Elastic::CI::API::Build, Elastic::CI::API::Timeline].each do |klass|
              klass.instance_exec @port, @logger do |port, logger|
                @client = ::Elasticsearch::Client.new url: "http://127.0.0.1:#{port}"
                @client.transport.logger = ( ENV['QUIET'] ? nil : logger )
              end
            end
          end

          context "`projects` resource" do
            should "return a list of all projects" do
              get '/projects'
              result = MultiJson.load(response.body)

              assert_equal 2, result.size
              assert_equal 'elasticsearch', result[0]['id']
              assert_equal 'lucene',        result[1]['id']
            end
          end

          context "`project` resource" do
            should "return single project" do
              get '/project/elasticsearch'
              result = MultiJson.load(response.body)

              assert_equal 'elasticsearch', result['id']
            end

            should "return builds for a project" do
              get '/project/elasticsearch?include=builds'
              result = MultiJson.load(response.body)

              assert_equal 15, result['builds'].size
              assert_equal 'SUCCESS', result['builds'].first['status']
            end
          end

          context "`builds` resource" do
            should "return a list of builds" do
              get '/builds'
              result = MultiJson.load(response.body)

              assert_equal 20, result.size # 15 ES + 5 L
            end

            should "return a list of builds filtered with criteria" do
              get '/builds?project_id=elasticsearch&status=FAILURE'
              result = MultiJson.load(response.body)

              assert_equal 1, result.size # 1 FAILURE only
            end
          end

          context "`build` resource" do
            should "return a single build" do
              get '/build/20160325131608-FC4FA3AB'
              result = MultiJson.load(response.body)

              assert_equal 'SUCCESS', result['process']['status']
              assert_equal '2016-03-25T13:16:09.328Z', result['process']['time-start']
            end
          end

          context "`timeline` resource" do
            should "return combined statistics for all projects" do
              get '/timeline'
              result = MultiJson.load(response.body)

              assert_equal 1, result.size
              assert_equal ['_all'], result.map { |d| d['project_id'] }

              assert_equal 25, result[0]['builds'].size # First document is from 2016-03-01
              assert_equal 1, result[0]['builds'][0]['builds']
              refute_nil result[0]['builds'][0]['avg_duration']
            end

            should "return statistics for a single project" do
              get '/timeline?project_ids=lucene'
              result = MultiJson.load(response.body)

              assert_equal 1, result.size
              assert_equal ['lucene'], result.map { |d| d['project_id'] }

              assert_equal 5, result[0]['builds'][0]['builds']

              refute_nil result[0]['builds'][0]['avg_duration']
            end

            should "return statistics for a list of projects" do
              get '/timeline?project_ids=elasticsearch,lucene'
              result = MultiJson.load(response.body)

              assert_equal 2, result.size
              assert_equal ['elasticsearch', 'lucene'], result.map { |d| d['project_id'] }

              assert_equal 14, result[0]['builds'].last['builds'] # 14 ES documents from 2016-03-25
              assert_equal 5,  result[1]['builds'].last['builds'] # 5 L documents from 2016-03-25

              refute_nil result[0]['builds'][0]['avg_duration']
            end

            should "allow to customize granularity" do
              get '/timeline?granularity=month'
              result = MultiJson.load(response.body)

              assert_equal 1, result[0]['builds'].size # Only one month in fixtures
              assert_equal ['2016-03-01'], result[0]['builds'].map { |b| b['date'] }
            end

            should "return additional metrics" do
              get '/timeline?properties=sys.os'
              result = MultiJson.load(response.body)

              assert_equal 'CentOS', result[0]['builds'][0]['sys.os'][0]['name']
            end
          end
        end
      end
    end
  end
end
