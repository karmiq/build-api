require 'simplecov'
SimpleCov.start do
  add_filter 'test/'
end

ENV['RACK_ENV'] = 'test'

require 'pathname'

require 'minitest/autorun'
require 'minitest/reporters'
require 'shoulda/context'
require 'mocha/mini_test'

require 'ansi'
require 'elasticsearch/extensions/test/startup_shutdown'
require 'elasticsearch/extensions/test/cluster'

FIXTURES = Pathname.new(File.expand_path('../fixtures', __FILE__))

Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new
